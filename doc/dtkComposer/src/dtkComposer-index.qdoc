/*!
    \page dtkcomposer-index.html
    \title Composer Module
    \brief A module which provides a set of C++ technologies for building visual programs

    \section1 Introduction

    Composer layer provides a visual programming framework that
    enables not only to manipulate fundamental concepts defined in
    the Core layer (data, process, view) but also to integrate some
    features from other layers (e.g. dtkDistributed layer or
    dtkLinearAlgebraSparse layer). As the Core layer, it can be
    extended in any application logic so as to handle additional
    concepts.

    Composer layer is made up of three main frameworks, namely logic,
    scene and evaluation, that are at the heart of the visual
    programming concept. Three additional frameworks (command,
    interfaces, integration) provides peripheral tools that extend
    functionalities both in terms of programmation and interaction.

    \section1 Logic framework

    This framework defines the logic of the data flow through two main
    concepts: the concept of node and the concept of transmitter.
    dtkComposerNode A node is a more or less atomic piece of the data
    flow that embeds a given logic. Three kind of nodes exist:

    \list

    \li Leaf nodes wrap logics provided by other layers of dtk or
    application logics (e.g. dtkCore or numCore).

    \li Composite nodes enables to define sub part of composition in
    order to provide some services or just to simplify the data flow
    for the end user.

    \li Control nodes define control structure that allow to perform
    loop or switch between some parts of the composition.

    \endlist

    \section1 dtkComposerTransmitter

    Transmitters enable data transmission between nodes. Transmission
    can be carried out according to two modes, namely a strong types
    data mode based on templates and a weak typed data mode based on
    QVariant. Furthermore, four types of transmitters exist:

    \list
    \li dtkComposerTransmitterEmitter enables to send data according
    to the strong types way.
    \li dtkComposerTransmitterReceiver enables to receive data
    according to the strong types way.
    \li dtkComposerTransmitterProxy is used as relay between emitters
    and receivers, it is not typed at all.
    \li dtkComposerTransmitterVariant can play the role of both an
    emitter or a receiver. It is weakly typed, that is to say it can
    connect two nodes that exchange data of not exactly the same type
    (e.g. integer and real).

    \endlist

    \section1 Extension

    The composer can be extended by implementing a
    dtkComposerExtension plugin. You have to implement the \c {
    extend(dtkComposerFactory *f)} method that records new nodes to
    the composer factory, and the \c { initialize()} method of the
    plugin should extend the factory and initialize its own factories,
    like this:

    \code
    void dtkLinearAlgebraSparseComposerExtensionPlugin::initialize(void)
    {
        dtkComposer::extension::pluginFactory().record("dtkLinearAlgebraSparse", dtkLinearAlgebraSparseComposerCreator);
        dtkComposerExtension *extension = dtkComposer::extension::pluginFactory().create("dtkLinearAlgebraSparse");
        bool verbose = dtkComposer::extension::pluginManager().verboseLoading();
        dtkLinearAlgebraSparse::pluginManager::setVerboseLoading(verbose);
        extension->extend(&(dtkComposer::node::factory()));
        dtkLinearAlgebraSparse::pluginManager::initialize();
    }
    \endcode

    The final step is to configure the settings file dtk-composer.ini,
    in order to find the extension plugins from the different layes,
    like this:
    \code
    [extension]
    plugins=/home/<username>/git/dtk-imaging/build/plugins:/home/<username>/git/dtk-linear-algebra-sparse/build/plugins:/home/<username>/git/dtk-discrete-geometry/build/plugins:
    \endcode

    \section1 Shortcuts

    To facilitate the use of the composer, some shortcuts can be employed:

    \list
    \li [ CTRL ] [ R ] : Evaluate a composition
    \li [ CTRL ] [ G ] : Group selected nodes into a composite node
    \li [ CTRL ] [ U ] : Ungroup a composite node
    \li [ CTRL ] [ Z ] : Undo
    \li [ CTRL ] [ backspace ] : delete a selected node
    \li [ CTRL ] [ O ] : load a composition
    \li [ CTRL ] [ SHIFT ] [ 1 ] : load last composition
    \li [ CTRL ] [ S ] : save a composition
    \endlist

    \section1 Reference
    These are links to the API reference materials.
    \list
    \li \l{Composer C++ Classes}{C++ Classes API}
    \endlist

    \sa {All Modules}
*/
