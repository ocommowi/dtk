dtk is a meta-platform for modular scientific platform development.

# Requisites

For mpi to provide C++ bindings on mac, use:

    $ brew install openmpi --c++11 --with-cxx-bindings

# Tools

To fix wrong export include directives, use:

    $ perl -pi -w -e "s/#include \"(.*Export\.h)\"/#include <\1>/g" <LAYER>/*
