// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkCorePluginFactoryTest.h"
#include "dtkCorePluginFactoryTestConcept.h"

#include <QtTest>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkCorePluginFactoryTestConceptPlugin : public dtkCorePluginFactoryTestConcept
{
public:
    ~dtkCorePluginFactoryTestConceptPlugin(void) {}

public:
    QString name(void) const { return "dtkCorePluginFactoryTestConceptPlugin"; }
};

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class dtkCorePluginFactoryTestCasePrivate
{
public:
    dtkCorePluginFactoryTestConceptPluginFactory factory;
};

dtkCorePluginFactoryTestConcept *dtkCorePluginFactoryTestConceptPluginCreator(void)
{
    return new dtkCorePluginFactoryTestConceptPlugin;
}

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkCorePluginFactoryTestCase::dtkCorePluginFactoryTestCase(void) : d(new dtkCorePluginFactoryTestCasePrivate)
{

}

dtkCorePluginFactoryTestCase::~dtkCorePluginFactoryTestCase(void)
{
    delete d;
}

void dtkCorePluginFactoryTestCase::initTestCase(void)
{
    d->factory.record("dtkCorePluginFactoryTestConceptPlugin", dtkCorePluginFactoryTestConceptPluginCreator);
}

void dtkCorePluginFactoryTestCase::init(void)
{

}

void dtkCorePluginFactoryTestCase::testBasic(void)
{
    dtkCorePluginFactoryTestConcept *concept = d->factory.create("dtkCorePluginFactoryTestConceptPlugin");
    QVERIFY(concept);
    QCOMPARE(QString("dtkCorePluginFactoryTestConceptPlugin"), concept->name());
}

void dtkCorePluginFactoryTestCase::cleanup(void)
{

}

void dtkCorePluginFactoryTestCase::cleanupTestCase(void)
{

}

DTKTEST_MAIN_NOGUI(dtkCorePluginFactoryTest, dtkCorePluginFactoryTestCase)

//
// dtkCorePluginFactoryTest.cpp ends here
