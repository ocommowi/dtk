/* dtkComposerNodeTest.cpp ---
 *
 * Author: Nicolas Niclausse
 */

#include "dtkComposerGraphTest.h"

#include <dtkComposer>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

void dtkComposerGraphTestCase::initTestCase(void)
{
}

void dtkComposerGraphTestCase::init(void)
{
    dtkComposer::node::initialize();

    m_factory = &(dtkComposer::node::factory());
    m_graph = new dtkComposerGraph;

    m_reader = new dtkComposerReaderNoScene;
    m_reader->setFactory(m_factory);
    m_reader->setGraph(m_graph);


    dtkLogger::instance().attachConsole();
    dtkLogger::instance().setLevel("trace");

}

void dtkComposerGraphTestCase::testReaderFor(void)
{
    bool ret = m_reader->read("./test-pi.dtk");
    QCOMPARE(ret, true);

    dtkComposerWriterNoScene writer;
    writer.setGraph(m_reader->nodeGraph());
    dtkDebug() << "writer node graph:";

    QTemporaryFile file("dtk-composer-XXXXXX.dtk");

    if (file.open()) {

        ret = writer.write(file.fileName());
        QCOMPARE(ret, true);

    } else {
        QFAIL("Can't create temporary file!");
    }

}


void dtkComposerGraphTestCase::cleanupTestCase(void)
{

}

void dtkComposerGraphTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkComposerGraphTest, dtkComposerGraphTestCase)

//#include "dtkComposerGraphTest.moc"
