// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#include <Python.h>

#include "dtkScriptInterpreterPython.h"

#include <dtkLog/dtkLog.h>
#include <dtkLog/dtkLogger.h>

#include <QtCore>


// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

static const QString dtkScriptInterpreterPythonRedirector_declare =
    "import sys\n"
    "\n"
    "class Redirector:\n"
    "    def __init__(self):\n"
    "        self.data = ''\n"
    "    def write(self, stuff):\n"
    "        self.data+= stuff\n";

static const QString dtkScriptInterpreterPythonRedirector_define =
    "redirector = Redirector()\n"
    "sys.stdout = redirector\n"
    "sys.stderr = redirector\n";

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkScriptInterpreterPythonPrivate
{
public:
    QString buffer;
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

dtkScriptInterpreterPython *dtkScriptInterpreterPython::instance(void)
{
    if (!s_instance) {
        s_instance = new dtkScriptInterpreterPython;
    }
    return s_instance;
}

dtkScriptInterpreterPython *dtkScriptInterpreterPython::s_instance = nullptr;


dtkScriptInterpreterPython::dtkScriptInterpreterPython(void) : dtkScriptInterpreter(), d(new dtkScriptInterpreterPythonPrivate)
{

}

dtkScriptInterpreterPython::~dtkScriptInterpreterPython(void)
{
    delete d;
    d = nullptr;
}

void dtkScriptInterpreterPython::init(void)
{
    Py_Initialize();

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "inria", "dtk-script");
    settings.beginGroup("modules");
    QString paths = settings.value("path").toString();
    settings.endGroup();

    PyRun_SimpleString("import sys");

    auto path_list = paths.split(":", QString::SkipEmptyParts);
    for (QString path : path_list) {
        PyRun_SimpleString(qPrintable(QString("sys.path.append('%1')").arg(path)));
    }

    settings.beginGroup("init");
    QString init = settings.value("script").toString();
    settings.endGroup();

    if (!init.isEmpty()) {
        PyRun_SimpleString(qPrintable(QString("execfile('%1')").arg(init)));
    } else {
        dtkWarn() << Q_FUNC_INFO << "No init function";
    }
}

void dtkScriptInterpreterPython::release(void)
{
    Py_Finalize();
}

QString dtkScriptInterpreterPython::interpret(const QString& command, int *stat)
{
    QString statement = command;

    if (command.endsWith(":")) {
        if (!d->buffer.isEmpty()) {
            d->buffer.append("\n");
        }
        d->buffer.append(command);
        return QString();
    }

    if (!command.isEmpty() && command.startsWith(" ")) {
        if (!d->buffer.isEmpty()) {
            d->buffer.append("\n");
        }
        d->buffer.append(command);
        return QString();
    }

    if (command.isEmpty() && !d->buffer.isEmpty()) {
        if (!d->buffer.isEmpty()) {
            d->buffer.append("\n");
        }
        statement = d->buffer;
        d->buffer.clear();
    }

    if (statement.isEmpty()) {
        return QString();
    }

    PyObject *module = PyImport_AddModule("__main__");

    switch (PyRun_SimpleString(qPrintable(statement))) {
    case  0: *stat = Status_Ok;    break;
    case -1: *stat = Status_Error; break;
    default: break;
    }

    PyErr_Print();

    return QString();
}

//
// dtkScriptInterpreterPython.cpp ends here
