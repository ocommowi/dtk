// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkScriptExport>

#include "dtkScriptInterpreter.h"

class DTKSCRIPT_EXPORT dtkScriptInterpreterPython : public dtkScriptInterpreter
{
    Q_OBJECT

public:
     static dtkScriptInterpreterPython *instance();

protected:
     static dtkScriptInterpreterPython *s_instance;

public slots:
    QString interpret(const QString& command, int *stat);
    void init(void);
    void release(void);

private:
     dtkScriptInterpreterPython(void);
     ~dtkScriptInterpreterPython(void);

private:
    class dtkScriptInterpreterPythonPrivate *d;
};

//
// dtkScriptInterpreterPython.h ends here
